use entity::user;
use log::info;
use poise::serenity_prelude as serenity;
use sea_orm::prelude::DateTime;
use sea_orm::{ActiveModelTrait, EntityTrait, QueryFilter, Set};
pub type Error = Box<dyn std::error::Error + Send + Sync>;
pub type Context<'a> = poise::Context<'a, Data, Error>;
use sea_orm::ColumnTrait;
// User data, which is stored and accessible in all command invocations
pub struct Data {
    db_conn: DatabaseConnection,
}

use migration::{Migrator, MigratorTrait};

pub mod commands;

use commands::config_server::config_server;
use commands::prune::prune;
use commands::prune::prune_dry_run;
use commands::scan_all_messages::scan_all_messages;

use sea_orm::DatabaseConnection;

async fn event_handler(
    _ctx: &serenity::Context,
    event: &serenity::FullEvent,
    _framework: poise::FrameworkContext<'_, Data, Error>,
    user_data: &Data,
) -> Result<(), Error> {
    match event {
        serenity::FullEvent::Ready { data_about_bot } => {
            info!("{} is connected!", data_about_bot.user.name);
            info!("In {} guilds", data_about_bot.guilds.len());
        }
        serenity::FullEvent::Message { new_message } => {
            let db_conn = user_data.db_conn.clone();

            let author_id = new_message.author.id.get() as i64;
            let timestamp = new_message.timestamp;

            let existing_user: Option<user::Model> =
                user::Entity::find_by_id(author_id).one(&db_conn).await?;

            if existing_user.is_none() {
                let user_entry = user::ActiveModel {
                    id: Set(author_id),
                    last_message_timestamp: Set(DateTime::from_timestamp_opt(
                        timestamp.timestamp(),
                        timestamp.timestamp_subsec_nanos(),
                    )
                    .expect("Couldn't convert timestamp")),
                    guild_id: Set(new_message.guild_id.unwrap().get() as i64),
                };

                let _ = user::Entity::insert(user_entry).exec(&db_conn).await?;
            } else {
                let mut existing_user: user::ActiveModel = existing_user.unwrap().into();

                existing_user.last_message_timestamp = Set(DateTime::from_timestamp_opt(
                    timestamp.timestamp(),
                    timestamp.timestamp_subsec_nanos(),
                )
                .expect("Couldn't convert timestamp"));

                let _: user::Model = existing_user.update(&db_conn).await?;
            }
        }
        serenity::FullEvent::GuildMemberRemoval {
            guild_id,
            user,
            member_data_if_available: _,
        } => {
            let db_conn = user_data.db_conn.clone();

            user::Entity::delete_many()
                .filter(user::Column::GuildId.eq(guild_id.get() as i64))
                .filter(user::Column::Id.eq(user.id.get() as i64))
                .exec(&db_conn)
                .await?;
        }
        _ => {}
    }

    Ok(())
}

#[poise::command(prefix_command)]
async fn register(ctx: Context<'_>) -> Result<(), Error> {
    poise::builtins::register_application_commands_buttons(ctx).await?;
    Ok(())
}

#[tokio::main]
async fn main() {
    env_logger::init();

    let connection = sea_orm::Database::connect("sqlite://lurker.db")
        .await
        .expect("Can't open DB");
    let _ = Migrator::up(&connection, None).await;

    let token = std::env::var("DISCORD_TOKEN")
        .expect("Missing `DISCORD_TOKEN` env var, see README for more information.");
    let intents = serenity::GatewayIntents::non_privileged();

    let framework = poise::Framework::builder()
        .options(poise::FrameworkOptions {
            commands: vec![
                register(),
                scan_all_messages(),
                config_server(),
                prune(),
                prune_dry_run(),
            ],
            event_handler: |ctx, event, framework, user_data| {
                Box::pin(event_handler(ctx, event, framework, user_data))
            },
            ..Default::default()
        })
        .setup(move |_ctx, _ready, _framework| {
            Box::pin(async move {
                Ok(Data {
                    db_conn: connection,
                })
            })
        })
        .build();

    let client = serenity::ClientBuilder::new(token, intents)
        .framework(framework)
        .await;

    client.unwrap().start().await.unwrap();
}
