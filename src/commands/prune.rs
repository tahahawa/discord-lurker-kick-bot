use crate::Context;
use crate::Error;

use chrono::Duration;

use chrono::Utc;
use entity::guild;
use entity::user;

use log::info;
use log::warn;
use sea_orm::ColumnTrait;
use sea_orm::EntityTrait;
use sea_orm::QueryFilter;

#[poise::command(slash_command)]
pub async fn prune(ctx: Context<'_>) -> Result<(), Error> {
    let user_can_call = ctx
        .author_member()
        .await
        .expect("Failed to get calling user")
        .permissions(ctx)
        .expect("Failed to get calling user permissions")
        .manage_guild();

    if !user_can_call {
        ctx.say("You need to have Manage Server permission to use this")
            .await?;
        return Err(Error::from("Bad permissions"));
    }

    let db_conn = ctx.data().db_conn.clone();

    let partial_guild = ctx
        .partial_guild()
        .await
        .expect("Server config used outside of guild");

    let guild_id = ctx.guild_id().expect("Server config used outside of guild");

    let existing_guild: Option<guild::Model> =
        guild::Entity::find_by_id(guild_id).one(&db_conn).await?;

    if existing_guild.is_none() {
        ctx.say("Please configure your server config before attempting to prune")
            .await?;
        return Err(Error::from("No server config"));
    }

    let existing_guild = existing_guild.unwrap();

    let config = existing_guild.config.as_object().unwrap();

    let mut role_to_modify = None;

    if config["prune_type"] == "add_role" || config["prune_type"] == "remove_role" {
        if config["role_to_modify"].as_str().unwrap().trim().is_empty() {
            ctx.say(
                "Please configure your server config's role to modify before attempting to prune",
            )
            .await?;
            return Err(Error::from("Bad server config"));
        }

        let role_to_modify_opt =
            partial_guild.role_by_name(config["role_to_modify"].as_str().unwrap());

        if role_to_modify_opt.is_none() {
            ctx.say("Couldn't find role to remove, please double check your config")
                .await?;
            return Err(Error::from("Bad server config"));
        }

        info!("Role ID: {}", role_to_modify_opt.unwrap().id);

        role_to_modify = role_to_modify_opt;

        if role_to_modify.is_none() {
            ctx.say("Couldn't find role to remove, please double check your config")
                .await?;
            return Err(Error::from("Bad server config"));
        }
    }

    ctx.defer().await?;

    let cutoff_datetime = Utc::now() - Duration::days(config["days_until_prune"].as_i64().unwrap());

    let users_to_prune = user::Entity::find()
        .filter(user::Column::LastMessageTimestamp.lt(cutoff_datetime))
        .filter(user::Column::GuildId.eq(guild_id.get() as i64))
        .all(&db_conn)
        .await?;

    let users_to_prune_len = users_to_prune.len();

    for u in users_to_prune {
        if config["prune_type"] == "kick" {
            partial_guild
                .kick_with_reason(&ctx.http(), u.id as u64, "Kicking lurkers")
                .await?;
        } else if config["prune_type"] == "add_role" {
            let member_res = partial_guild.member(ctx.http(), u.id as u64).await;
            if let Ok(member) = member_res {
                if let Some(role) = role_to_modify {
                    member.add_role(ctx.http(), role.id).await?;
                    info!(
                        "Added role {} to {} in {}",
                        role.name, member.user.name, partial_guild.name
                    )
                } else {
                    warn!("Couldn't get role")
                }
            } else {
                warn!("Couldn't get member from partial guild")
            }
        } else if config["prune_type"] == "remove_role" {
            let member_res = partial_guild.member(ctx.http(), u.id as u64).await;
            if let Ok(member) = member_res {
                if let Some(role) = role_to_modify {
                    member
                        .remove_role(ctx.http(), role_to_modify.unwrap().id)
                        .await?;
                    info!(
                        "Removed role {} from {} in {}",
                        role.name, member.user.name, partial_guild.name
                    )
                }
            } else {
                warn!("Couldn't get member from partial guild")
            }
        }
    }

    ctx.say(format!("Pruned {} users", users_to_prune_len))
        .await?;

    Ok(())
}

#[poise::command(slash_command)]
pub async fn prune_dry_run(ctx: Context<'_>) -> Result<(), Error> {
    let user_can_call = ctx
        .author_member()
        .await
        .expect("Failed to get calling user")
        .permissions(ctx)
        .expect("Failed to get calling user permissions")
        .manage_guild();

    if !user_can_call {
        ctx.say("You need to have Manage Server permission to use this")
            .await?;
        return Err(Error::from("Bad permissions"));
    }

    let db_conn = ctx.data().db_conn.clone();

    let guild_id = ctx.guild_id().expect("Server config used outside of guild");

    let existing_guild: Option<guild::Model> =
        guild::Entity::find_by_id(guild_id).one(&db_conn).await?;

    if existing_guild.is_none() {
        ctx.say("Please configure your server config before attempting to prune")
            .await?;
        return Err(Error::from("No server config"));
    }

    let existing_guild = existing_guild.unwrap();

    let config = existing_guild.config.as_object().unwrap();

    ctx.defer().await?;

    let cutoff_datetime = Utc::now() - Duration::days(config["days_until_prune"].as_i64().unwrap());

    let users_to_prune = user::Entity::find()
        .filter(user::Column::LastMessageTimestamp.lt(cutoff_datetime))
        .filter(user::Column::GuildId.eq(guild_id.get() as i64))
        .all(&db_conn)
        .await?;

    ctx.say(format!(
        "Pruning would affect these {} people:",
        users_to_prune.len()
    ))
    .await?;

    let mut users_to_prune_str: Vec<String> = Vec::new();
    for u in users_to_prune {
        let discord_user_res = guild_id.member(ctx, u.id as u64).await;
        if discord_user_res.is_ok() {
            let discord_user = discord_user_res.unwrap();
            let discord_user_str = discord_user.user.name;
            users_to_prune_str.push(discord_user_str);
        }
    }

    let dst: Vec<&[String]> = users_to_prune_str.chunks(10).collect();

    for user_vec_chunk in dst {
        ctx.say(user_vec_chunk.join("\n")).await?;
    }

    Ok(())
}
