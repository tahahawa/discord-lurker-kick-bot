use crate::Context;
use crate::Error;
use entity::user;
use log::*;
use sea_orm::prelude::DateTime;
use sea_orm::{ActiveModelTrait, EntityTrait, Set};
use serenity::all::GetMessages;

#[poise::command(slash_command, prefix_command)]
pub async fn scan_all_messages(ctx: Context<'_>) -> Result<(), Error> {
    let user_can_call = ctx
        .author_member()
        .await
        .expect("Failed to get calling user")
        .permissions(ctx)
        .expect("Failed to get calling user permissions")
        .manage_guild();

    if !user_can_call {
        ctx.say("You need to have Manage Server permission to use this")
            .await?;
        return Err(Error::from("Bad permissions"));
    }

    ctx.defer().await?;
    ctx.say("Scanning...").await?;

    let db_conn = ctx.data().db_conn.clone();

    let guild = ctx
        .partial_guild()
        .await
        .expect("Failed to get partial guild");
    let channels = guild.channels(&ctx.http()).await?;

    // TODO: use MessageIter, and add implement after for it
    for chan in channels {
        let mut _messages = Vec::new();

        let channel_name = chan.1.name;

        info!("{:?}", channel_name);

        if chan.1.bitrate.is_some() {
            continue;
        }

        let mut biggest_id = {
            if chan.1.last_message_id.is_none() {
                info!("skipped, no latest message exists");
                continue;
            } else {
                chan.1.last_message_id.unwrap()
            }
        };

        let r#try = chan
            .0
            .messages(
                &ctx.http(),
                GetMessages::new().before(biggest_id).limit(100),
            )
            .await;
        match r#try {
            Err(e) => warn!("error getting messages {:?}", e),
            _ => _messages = r#try.unwrap(),
        }

        info!("Scanning channel: {channel_name}");
        // ctx.say(format!("Scanning channel: {channel_name}")).await?;

        while !_messages.is_empty() {
            let _ = chan.0.broadcast_typing(&ctx.http()).await;
            info!(
                "scanning {} messages from #{} on {}",
                _messages.len(),
                channel_name,
                guild.name
            );
            let message_vec = _messages.to_vec();
            for message in message_vec {
                if message.id < biggest_id {
                    biggest_id = message.id;
                }

                let author_id = message.author.id.get() as i64;
                let timestamp = message.timestamp;
                let existing_user: Option<user::Model> =
                    user::Entity::find_by_id(author_id).one(&db_conn).await?;

                let is_member = guild.member(ctx, author_id as u64).await.is_ok();

                if existing_user.is_none() && is_member {
                    let user_entry = user::ActiveModel {
                        id: Set(author_id),
                        last_message_timestamp: Set(DateTime::from_timestamp_opt(
                            timestamp.timestamp(),
                            timestamp.timestamp_subsec_nanos(),
                        )
                        .expect("Couldn't convert timestamp")),
                        guild_id: Set(guild.id.get() as i64),
                    };

                    let _ = user::Entity::insert(user_entry).exec(&db_conn).await?;
                } else if existing_user.is_some() && is_member {
                    let mut existing_user: user::ActiveModel = existing_user.unwrap().into();
                    if timestamp.timestamp()
                        > existing_user.last_message_timestamp.unwrap().timestamp()
                    {
                        existing_user.last_message_timestamp = Set(DateTime::from_timestamp_opt(
                            timestamp.timestamp(),
                            timestamp.timestamp_subsec_nanos(),
                        )
                        .expect("Couldn't convert timestamp"));

                        let _: user::Model = existing_user.update(&db_conn).await?;
                    }
                }

                // debug!("message: {:?}", message);
            }

            let r#try = chan
                .0
                .messages(
                    &ctx.http(),
                    GetMessages::new().before(biggest_id).limit(100),
                )
                .await;
            match r#try {
                Err(e) => warn!("error getting messages {:?}", e),
                _ => _messages = r#try.unwrap(),
            }

            //println!("{:?}", _messages);
        }
    }
    info!("Scanned all messages for {:?}", guild.name);

    ctx.say("Scanned all messages").await?;

    Ok(())
}
