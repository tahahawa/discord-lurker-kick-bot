use crate::Context;
use crate::Error;
use entity::guild;

use log::info;
use poise::futures_util::Stream;
use poise::futures_util::StreamExt;
use sea_orm::ActiveModelTrait;
use sea_orm::EntityTrait;
use sea_orm::Set;
use serde_json::json;
use serenity::futures;

async fn autocomplete_prune_type<'a>(
    _ctx: Context<'a>,
    partial: &'a str,
) -> impl Stream<Item = String> + 'a {
    futures::stream::iter(&["kick", "add_role", "remove_role"])
        .filter(move |name| futures::future::ready(name.starts_with(partial)))
        .map(|name| name.to_string())
}

#[poise::command(slash_command)]
pub async fn config_server(
    ctx: Context<'_>,
    #[description = "Days until prune"] days_until_prune: i64,
    #[description = "Prune type"]
    #[autocomplete = "autocomplete_prune_type"]
    prune_type_opt: Option<String>,
    #[description = "Role to modify"] role_to_modify: Option<String>,
) -> Result<(), Error> {
    let user_can_call = ctx
        .author_member()
        .await
        .expect("Failed to get calling user")
        .permissions(ctx)
        .expect("Failed to get calling user permissions")
        .manage_guild();

    if !user_can_call {
        ctx.say("You need to have Manage Server permission to use this")
            .await?;
        return Err(Error::from("Bad permissions"));
    }

    let db_conn = ctx.data().db_conn.clone();

    let prune_type = {
        if prune_type_opt.is_none() {
            "kick".to_owned()
        } else {
            prune_type_opt.unwrap()
        }
    };

    let config_json = json!({
        "days_until_prune": days_until_prune,
        "prune_type": prune_type,
        "role_to_modify": role_to_modify.unwrap_or("".to_owned()),
    });

    let guild_id = ctx
        .guild_id()
        .expect("Server config used outside of guild")
        .get() as i64;

    let existing_guild: Option<guild::Model> =
        guild::Entity::find_by_id(guild_id).one(&db_conn).await?;

    if existing_guild.is_none() {
        let guild_entry = guild::ActiveModel {
            id: Set(guild_id),
            config: Set(config_json),
        };

        let _ = guild::Entity::insert(guild_entry).exec(&db_conn).await?;
    } else {
        let mut existing_guild: guild::ActiveModel = existing_guild.unwrap().into();

        existing_guild.config = Set(config_json);

        let _: guild::Model = existing_guild.update(&db_conn).await?;
    }

    info!("Config saved for Guild {}", ctx.guild().unwrap().name);

    ctx.say("Config saved").await?;

    Ok(())
}
